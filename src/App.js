import {ThemeProvider, CssBaseline, Grid} from "@mui/material";
import theme from "./theme";

import { Routes, Route } from "react-router-dom";

import Home from './components/Home'
import Bookmarks from './components/Bookmarks'
import Definition from "./components/Definition";



const App =()=>{
  return(
      <ThemeProvider theme={theme}>
          <CssBaseline />
          <Grid container>
              <Grid item xs={12} sx={{ p:2}}>
                 <Routes>
                     <Route path="/" element={<Home />} />
                     <Route path="/bookmarks" element={<Bookmarks />} />
                     <Route path="/search/:words" element={<Definition />} />
                 </Routes>

              </Grid>
          </Grid>
      </ThemeProvider>
  )
}

export default App