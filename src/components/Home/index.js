import {useState} from 'react'

import {Box, Typography, FilledInput, IconButton } from "@mui/material"
import { Bookmark as BookmarkIcon} from '@mui/icons-material'
import SearchIcon from '@mui/icons-material/Search';
import {useNavigate} from "react-router-dom";

const Home=()=>{
    const [word, setWord] = useState('')
    const history = useNavigate()

    const handleSubmit = (event) => {
        event.preventDefault()
        const trimmedWord = word.trim().toLowerCase()
        if(!trimmedWord || trimmedWord.split(' ').length >1) return
        history(`/search/${word}`)
    }

    return(
            <Box sx={{
                display:'flex',
                flexDirection: 'column',
                alignItems:'center',
                justifyContent:'center',
                height:'100vh'
            }}>
                <img src='./img/book.png' alt="book"/>
                <Typography sx={{
                    mt:3,
                    mb:1,
                }} variant="h4">Dictionary</Typography>
                <Typography color="GrayText" sx={{
                    mb:1
                }}>Find meaning and save for quick reference</Typography>
                <Box sx={{
                    form:{
                        display:'flex',
                        flexDirection:'column',
                        width:'360px',
                    }
                }}>
                    <form onSubmit={handleSubmit}>
                        <FilledInput disableUnderline placeholder="Search word"
                                     value={word}
                                     onChange={event => setWord(event.target.value )}

                                     sx={{
                                         my:4,
                                         backgroundColor:'white',
                                         borderRadius:'5px',
                                         boxShadow:'0px 10px,25px rgba(0,0,0,0.5)',
                                         '& .MuiInputBase-input':{
                                             padding: "12px"
                                         }
                                     }}
                                     startAdornment={<SearchIcon color="primary"/>}
                                     fullWidth
                        />
                        <IconButton sx={{
                            margin:"0 auto",
                            background:theme=>theme.palette.pink ,
                            borderRadius: 2,
                            p:2,
                            color:'#fff',
                            boxShadow:'0px 10px 10px rgba(221,114,133,0.2)'
                        }}>
                            <BookmarkIcon />
                        </IconButton>
                    </form>

                </Box>

            </Box>
    )
}

export default Home