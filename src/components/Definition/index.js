import {useState, useEffect, Fragment} from "react";

import {Stack, Typography, Box, IconButton, Divider} from "@mui/material";
import {ArrowBack, BookmarkBorder, BorderAll,PlayArrow} from "@mui/icons-material";
import {useParams, useNavigate} from "react-router-dom"

import axios from 'axios'


const Definition=()=>{
    const params= useParams()
    let word = params.words

    const history = useNavigate()

    const [definitions, setDefinitions]= useState([])

    console.log(definitions)

    useEffect(()=>{
        const fetchDefinition = async ()=>{
           const resp =await axios.get(`https://api.dictionaryapi.dev/api/v2/entries/en/${word}`)
            setDefinitions(resp.data)
        }
        fetchDefinition()

    },[])

    return(
        <>
            <Stack direction="row" justifyContent="space-between">
                <IconButton onClick={()=>history(-1)}>
                    <ArrowBack />
                </IconButton>
                <IconButton>
                    <BookmarkBorder/>
                </IconButton>
            </Stack>
            <Stack alignItems="center" direction="row" justifyContent="space-around"
            sx={{
                mt:3,
                background:"linear-gradient(90.17deg, #191e5d 0.14%, #0f133a 98.58%)",
                boxShadow:'0px 10px 20px rgba(19, 23, 71, 0.25)',
                px:4,
                py:5,
                color:"white",
                borderRadius:2,
                display:'flex',
                flexDirection:'row',
                alignItems:'center',

            }}>
                <Typography sx={{
                    textTransform:'capitalize',
                    display:'block',
                    margin:"0 auto"
                }}variant="h5">
                    {word}
                </Typography>
                <IconButton sx={{
                    margin:"0 auto",
                    background:theme=>theme.palette.pink ,
                    borderRadius: 2,
                    p:1,
                    color:'#fff',
                }}><PlayArrow /></IconButton>
            </Stack>
            {definitions.map((def, i) =>
                    <Fragment key={i}>
                        <Divider />
                        {def.meanings}
                    </Fragment>
            )}
        </>
    )
}

export default Definition