import {createTheme} from "@material-ui/core";

export default createTheme({
    palette:{
        background:{
            default:'#F1F3F4'
        },
        primary:{
            main:'#14194c'
        },
        disabled:{
            main: '#123456'
        },
        pink:'linear-gradient(138.72deg, #dc8295 0%, #dc687c 95.83%)'

    },
    typography:{
        fontFamily:"Mulish, sans-serif",
        h4:{
            fontWeight:800
        },
        h5:{
            fontWeight:700
        }
    },

})
